<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct()
    {
        $this->middleware('auth');
    }

    /** Show form to add document */
    function add() {
        if (Auth::user()['verified']) {
            return view('docs/add');
        } else {
            return view('message', ['messages' => 'Please check your email and verify account.']);
        }
    }

    /** Save document */
    function store(Request $request) {
        $this->validate($request, [
            'alias' => 'required|string|max:255',
            'file' => 'required',
        ]);
        $file = $request->file();
        $file = array_pop($file);
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('docs/'), $fileName);
        require_once(app_path() . '\Doc.php');
        $doc = new \Doc($request->get('alias'), $fileName, file_get_contents(public_path('docs/') . $fileName));
        $doc->store();
        return view('message', ['messages' => 'File uploaded', 'status' => 'success']);
    }

    /** Show documents list */
    function listDocs(Request $request) {
        $allowedOrderFields = ['alias', 'date', 'author'];
        $params = $request->all();
        if (!empty($params['order']) && in_array($params['order'], $allowedOrderFields)) {
            $params['desc'] = (int) (isset($params['desc']) && $params['desc']);
            $params['urlParams'] = '&desc=' . (1 - $params['desc']);
        } else {
            $params['urlParams'] = '';
        }
        require_once(app_path() . '\Doc.php');
        $docs = \Doc::getDocList($params);
        $params['urlParams'] .= empty($params['search']) ? '' : '&search=' . $params['search'];
        return view('docs/list', ['docs' => $docs, 'params' => $params]);
    }

}

@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="row p-md-5 pt-5">
            <div class="col-md-3 col-sm-0"></div>
            <div class="col-md-6 col-sm-12"><h2 class="text-center"><span class="text-nowrap">Welcome to</span> <span class="text-nowrap"> «Astarus» Ltd</span><br>test application.</h2></div>
            <div class="col-md-3 col-sm-0"></div>
        </div>
    </div>
@endsection
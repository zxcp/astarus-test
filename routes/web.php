<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/verify/{id}/{token}', 'Auth\RegisterController@activate');

Route::get('add', 'HomeController@add')->name('add')->middleware('auth');;

Route::post('store', 'HomeController@store')->name('docStore');

Route::get('list', 'HomeController@listDocs')->name('listDocs');
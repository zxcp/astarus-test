<?php /** Created by Anton on 03.08.2018. */ ?>

@extends('layouts.app')

@section('content')
    <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <table class="table table-sm table-dark">
            <thead>
                <tr>
                    <th scope="col"><a href="/list/?order=alias{{ $params['urlParams'] }}">Name</a></th>
                    <th scope="col"><a href="/list/?order=date{{ $params['urlParams'] }}">Date</a></th>
                    <th scope="col"><a href="/list/?order=author{{ $params['urlParams'] }}">Author</a></th>
                    {{--<th scope="col"><a href="/list/">Size</a></th>--}}
                    <th class="text-center" scope="col"><a href="/list/">Download</a></th>
                </tr>
            </thead>
            <tbody>
                @foreach($docs as $doc)
                    <tr>
                        <td>{{ $doc->alias }}</td>
                        <td class="break-word">{{ $doc->date }}</td>
                        <td>{{ $doc->name }}</td>
                        {{--<td>{{ number_format(filesize(public_path('docs/') . $doc->file_name) / 1048576, 2) . ' MB' }}</td><!-- Storage::size() не видит файл -->--}}
                        <td class="text-center"><a href="/docs/{{ $doc->file_name }}" download>&#11123;</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-2"></div>
    </div>
@endsection
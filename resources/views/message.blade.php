<?php /** Created by Anton on 30.07.2018. */
/** @var array $messages */ ?>

@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="flex-center position-ref">
            <div class="alert alert-<?= isset($status) ? $status : 'danger' ?>" style="text-align: center">
                <?php foreach ((array) $messages as $msg): ?>
                    <h4><?= isset($status) ? ucfirst($status): 'Error' ?>: <?= $msg ?>.</h4>
                    <hr>
                <?php endforeach; ?>
                <p><a href="/" class="alert-link">return to home page.</a></p>
            </div>
        </div>
    </div>
@endsection
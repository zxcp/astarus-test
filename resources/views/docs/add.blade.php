<?php /** Created by Anton on 30.07.2018. */ ?>

@extends('layouts.app')

@section('content')
    <div class="content">
        @if ($errors->any())
            <div class="row justify-content-center align-items-center m-0">
                <div class="alert alert-danger">
                    <ul class="m-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="row justify-content-center align-items-center m-0">
            <div class="form">
                <form method="POST" action="{{ route('docStore') }}" enctype="multipart/form-data" files="true">
                    <div class="form-group">
                        <label for="alias">Document name</label>
                        <input type="text" class="form-control" id="alias" name="alias" required="required">
                    </div>
                    <div class="form-group">
                        <input type="file" name="file">
                    </div>
                    <button type="submit" class="btn d-block mx-auto">Upload</button>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection
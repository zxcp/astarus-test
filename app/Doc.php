<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/** Created by Anton on 30.07.2018. */

use Carbon\Carbon;

class Doc {
    protected $alias, $date, $author, $fileName, $file;

    function __construct($alias, $fileName, $file) {
        $this->alias = $alias;
//        $this->date = new DateTime('now', new DateTimeZone('Asia/Baku'));
        $this->date = Carbon::now()->addHours(4);//GMT +4
        $this->author = Auth::user()->id;
        $this->fileName = $fileName;
        $this->file = $file;
    }

    /** Get Docs from DB
     * @param array $params (Example: ["order" => "date", "desc" => 1, "search" => "porno", "urlParams" => "&desc=0"])
     * $return array
     */
    static function getDocList($params) {
        return DB::table('docs')
            ->join('users', 'docs.author', 'users.id')
            ->select('*')
            ->when(!empty($params['search']), function($query) use ($params) { return $query->where('alias', 'like', '%' . $params['search'] . '%'); })
            ->when($params['urlParams'], function($query) use ($params) { return $query->orderBy($params['order'], $params['desc'] ? 'desc' : 'asc'); })
            ->get();
    }

    /** Save Doc to DB */
    function store() {
        $validator = Validator::make(['alias' => $this->alias, 'file' => $this->file], [
            'alias' => 'required|string|max:255',
            'file' => 'required'
        ]);
        if ($validator->fails()) return redirect()->route('add')->withErrors($validator)->withInput();

        DB::table('docs')->insert([
            'alias' => $this->alias,
            'date' => $this->date,
            'author' => $this->author,
            'file_name' => $this->fileName,
//            'file' => $this->file
        ]);
    }

}
<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $data['token'] = md5($data['email']);
        $data['id_user'] = $user->id;

        Mail::send('emails.welcome', $data, function($message) use ($data) {
            $message->from('zxnightmare@gmail.com', "Test");
            $message->to($data['email'])->subject("Welcome to astarus test");
        });

        return $user;
    }

    /** Activate user by $token from email
     * @param int $idUser
     * @param string $token
     */
    protected function activate($idUser, $token) {
        $user = User::findOrFail($idUser);//findOrFail не дурно бы заменить

        if (md5($user->email) == $token) {
            $user->verified = 1;
            $user->save();
            return redirect('/');
        } else {
            return view('message', ['messages' => 'The token is wrong']);
        }
    }

    /** Override ot disable auto login after registration and show message */
    public function register(Request $request) {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return view('message', ['messages' => 'Please check and confirm your email', 'status' => 'success']);
    }

}